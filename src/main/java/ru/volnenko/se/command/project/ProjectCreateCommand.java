package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.controller.CommandEvent;

/**
 * @author Denis Volnenko
 */

@Component
public final class ProjectCreateCommand extends AbstractCommand {

    private IProjectRepository projectRepository;

    @Autowired
    public ProjectCreateCommand(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = nextLine();
        projectRepository.createProject(name);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    @EventListener
    public void receiveEvent(CommandEvent event) throws Exception {
        if(command().equalsIgnoreCase(event.getCliCommand())){
            execute();
        }
    }

}
