package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.controller.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public final class TaskCreateCommand extends AbstractCommand {

    private ITaskRepository taskRepository;

    @Autowired
    public TaskCreateCommand(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = nextLine();
        taskRepository.createTask(name);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    @EventListener
    public void receiveEvent(CommandEvent event) throws Exception {
        if(command().equalsIgnoreCase(event.getCliCommand())){
            execute();
        }
    }

}
