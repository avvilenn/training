package ru.volnenko.se.command;

import java.util.List;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.controller.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component("abstractCommand")
public abstract class AbstractCommand {

    @Autowired
    protected List<AbstractCommand> allCommands;

    public abstract void execute() throws Exception;

    public abstract String command();

    public abstract String description();

    protected String nextLine() {
        return new Scanner(System.in).nextLine();
    }

    protected Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    public abstract void receiveEvent(CommandEvent event) throws Exception;
}
