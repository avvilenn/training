package ru.volnenko.se.command.system;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.controller.CommandEvent;

/**
 * @author Denis Volnenko
 */

@Component
@Order(1)
@Lazy(false)
public class HelpCommand extends AbstractCommand{

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    @EventListener
    public void receiveEvent(CommandEvent event) throws Exception {
        if(command().equalsIgnoreCase(event.getCliCommand())){
            execute();
        }
    }

    @Override
    public void execute() {
        System.out.println(command()+ ": " + description());
        for (AbstractCommand command: allCommands) {
            System.out.println(command.command()+ ": " + command.description());
        }
    }
}
