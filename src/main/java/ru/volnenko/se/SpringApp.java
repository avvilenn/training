package ru.volnenko.se;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.volnenko.se.controller.Bootstrap;

public class SpringApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        Bootstrap bootstrap = (Bootstrap) context.getBean("Bootstrap");
        bootstrap.start();
    }
}
