package ru.volnenko.se.controller;

import org.springframework.context.ApplicationEvent;

public class CommandEvent extends ApplicationEvent {

    private String cliCommand;


    public CommandEvent(Object source, String cliCommand) {
        super(source);
        this.cliCommand = cliCommand;
    }

    public String getCliCommand() {
        return cliCommand;
    }
}
