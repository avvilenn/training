package ru.volnenko.se.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandAbsentException;

/**
 * @author Denis Volnenko
 */

@Component("Bootstrap")
public class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private List<AbstractCommand> allCommands;

    private final Scanner scanner = new Scanner(System.in);

    private ApplicationEventPublisher publisher;

    @Autowired
    public Bootstrap(List<AbstractCommand> allCommands, ApplicationEventPublisher publisher) {
        this.allCommands = allCommands;
        this.publisher = publisher;
    }

    public Bootstrap() {
    }


    @PostConstruct
    public void init() throws Exception {
        System.out.println("Calling init method....");
        if (allCommands == null || allCommands.size() == 0) throw new CommandAbsentException();
        System.out.println("Added commands ...." + allCommands.size());
    }

    public void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            CommandEvent event = new CommandEvent(this, command);
            publisher.publishEvent(event);
        }
    }

}
